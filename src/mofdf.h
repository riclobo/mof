//----------------------------------------------------------------------
// Class to parse a dielectric function optical function configuration
//----------------------------------------------------------------------
#ifndef MOFDF_H
#define MOFDF_H
//----------------------------------------------------------------------
#include <string>
//----------------------------------------------------------------------
#include "aliases.h"
#include "clparser.h"
//----------------------------------------------------------------------
class mofDF
{
  public:
    mofDF() { }       // Do nothing constructor -- useful for maps, vectors, etc.
    mofDF(clparser::strqueue &q,
          clparser::csr      ofn);

    clparser::csr Errors()  const { return errors; }

    double First() const { return w0; }
    double Last()  const { return wf; }
    double Step()  const { return dw; }

    unsigned Elements()         const { return MatElements; }
    unsigned OpticalFunctions() const { return OptFuncs; }

    clparser::csr OutputFile()  const { return OutFile; }
    clparser::csr Parameters()  const { return ParFile; }

    std::vector<double> Angles(unsigned Func) const;

  private:
    void Add_Angle(clparser::csr s, unsigned Func);
    void Add_ME(clparser::csr s);
    void Add_OF(clparser::csr s);
    void Def_Limits(clparser::csr s);
    void Def_Outfile(clparser::csr s);

                 double w0{10.0},
                        wf{1000.0},
                        dw{1.0};                 // Limits for calculations

               unsigned MatElements{al::nothing},  // Matrix elements to generate
                        OptFuncs{al::nothing};   // Optical functions to generate

            std::string errors{""},              // Eventual parsing error messages
                        OutFile,                 // Output file name
                        ParFile;                 // File containing the parameters

    std::vector<double> angs_R,                  // Angles for calculation of the reflectivity
                        angs_eta;                // Angles for calculation of the loss function
};
#endif // MOFDF_H

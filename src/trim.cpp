//----------------------------------------------------------------------
// Functions for trimming, reducing and similar string operations
//----------------------------------------------------------------------
#include <algorithm>
#include <string>
//----------------------------------------------------------------------
#include <iostream>
//----------------------------------------------------------------------
#include "trim.h"
//----------------------------------------------------------------------
// Trim leading spaces and tabs
//----------------------------------------------------------------------
std::string lobo::ltrim(const std::string &s)
{
  size_t First = s.find_first_not_of(" \t");

  if (First == std::string::npos)   // String is at best only blanks
    return "";

  return s.substr(First);
}
//----------------------------------------------------------------------
// Trim trailing spaces and tabs
//----------------------------------------------------------------------
std::string lobo::rtrim(const std::string &s)
{
  size_t Last = s.find_last_not_of(" \t");

  if (Last == std::string::npos)  // String is at best only blanks
    return "";

  return s.substr(0,Last+1);
}
//----------------------------------------------------------------------
// Trim leading and trailing spaces and tabs
//----------------------------------------------------------------------
std::string lobo::trim(const std::string &s)
{
  return lobo::rtrim( lobo::ltrim(s) );
}
//----------------------------------------------------------------------
// Erases "useless" spacers
//----------------------------------------------------------------------
std::string lobo::reduce(const std::string &s, const std::string &filler,
                         const std::string &spacer)
{
  std::string retval = lobo::trim(s);   // Trim the string

  // Deals with internal multiple spaces
  size_t Start = retval.find_first_of(spacer);

  while (Start != std::string::npos)
  {
    size_t End = retval.find_first_not_of(spacer,Start);
    size_t Range = End - Start;

    retval.replace(Start,Range,filler);

    size_t NewStart = Start + filler.length();
    Start = retval.find_first_of(spacer, NewStart);
  }

  retval = lobo::trim(retval);    // Trim again if spacer was separating spaces

  return retval;
}
//----------------------------------------------------------------------
// Erases all spaces and tabs unless inside quotes -- Assumes matching quotes
//----------------------------------------------------------------------
std::string lobo::smart_space_eraser(const std::string &s, char quote,
                                     bool ToLowerCase)
{
  std::string q{""},
              r{""};

  size_t p0 = 0;
  size_t p1 = s.find_first_of(quote);          // Look for " and...
  size_t p2 = s.find_first_of(quote, p1 + 1);  // ...its matching pair

  // If no (or just one) " is present does not do anything
  if (p1 == std::string::npos || p2 == std::string::npos)
    return s;

  do
  {
    // Extracts portion before ", eliminates its spaces and converts to lower case if asked
    q = s.substr(p0, p1 - p0);
    q.erase( std::remove_if(q.begin(), q.end(), ::isspace), q.end() );
    if (ToLowerCase)
      std::transform(q.begin(), q.end(), q.begin(), ::tolower);

    // Add cleaned portion and quoted text to return string
    r += q + s.substr(p1, p2 - p1 + 1);

    // Looks for the position of the next set of quotes
    p0 = p2 + 1;
    p1 = s.find_first_of(quote, p0);
    p2 = s.find_first_of(quote, p1 + 1);
  } while (p1 != std::string::npos && p2 != std::string::npos);


  // Add the portion past the last quote to the return string
  q = s.substr(p0);
  q.erase( std::remove_if(q.begin(), q.end(), ::isspace), q.end() );
  if (ToLowerCase)
    std::transform(q.begin(), q.end(), q.begin(), ::tolower);
  r += q;

  return r;
}

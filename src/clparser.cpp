//----------------------------------------------------------------------
// Functions to handle coupo file io routines
//----------------------------------------------------------------------
#include <algorithm>
#include <fstream>
//----------------------------------------------------------------------
//----------------------------------------------------------------------
#include "clparser.h"
#include "trim.h"
//----------------------------------------------------------------------
// Locally defined functions
//----------------------------------------------------------------------
// Cleans a line from useless spaces. Also checks that " are in pairs
// and eliminates multiple ;;;
//----------------------------------------------------------------------
bool CleanCheckString(std::string &s, bool ToLowerCase)
{
  size_t nquotes = std::count(s.begin(), s.end(), '\"');  // Counts "

  if ((nquotes % 2) == 1)   // Unmatching quotes
    return false;

  if (nquotes == 0)
  {
    s.erase( std::remove_if(s.begin(), s.end(), ::isspace), s.end() );
    if (ToLowerCase)   // Not case sensitive -- converts string to lower case
      std::transform(s.begin(), s.end(), s.begin(), ::tolower);
  }
  else
    s = lobo::smart_space_eraser(s, '\"', ToLowerCase);

  s = lobo::reduce(s, ";", ";");

  return true;
}
//----------------------------------------------------------------------
// Converts Block name to Function()
// Func       --> Func()
// Func_a     --> Func(a)
// Func_a.b.c --> Func(a,b,c)
//----------------------------------------------------------------------
std::string Block2Func(clparser::csr f)
{
  size_t p1 = f.find_last_of('_');            // Looks for last _
  std::string retval = f.substr(0, p1) +"(";  // Copies function name to return value

  if (p1 != std::string::npos)      // There is a parameter list
  {
    std::string pars   = f.substr(p1 + 1, f.length() - p1 - 1 );  // Function parameters
    std::replace(pars.begin(), pars.end(), '.', ',');             // Replaces '.' by ','
    retval += pars;
  }

  retval += ")";     // Adds closing parenthesis

  return retval;
}
//----------------------------------------------------------------------
// Converts Function() to Block name
// Func()      --> Func
// Func(a)     --> Func_a
// Func(a,b,c) --> Func_a.b.c
//----------------------------------------------------------------------
std::string Func2Block(clparser::csr f)
{
  size_t p1 = f.find('(');              // Finds (

  std::string retval = f.substr(0, p1),                         // Function name
              pars   = f.substr(p1 + 1, f.length() - p1 - 2 );  // Parameter list

  pars.erase( std::remove_if(pars.begin(), pars.end(), ::isspace), pars.end() );  // Erases all spaces
  std::replace(pars.begin(), pars.end(), ',', '.');                               // Replaces ',' by '.'

  if (!pars.empty())
    retval += '_' + pars;     // Adds occasional parameter list to name

  return retval;
}
//----------------------------------------------------------------------
// Public functions
//----------------------------------------------------------------------
// Parse a configuration file
//----------------------------------------------------------------------
bool clparser::Parse(csr FileName)
{
  bool retval = true;

  if (ReadFile(FileName))                   // Tries to open file
  {
    if (!CheckSyntax())   retval = false;   // Checks that syntax is ok
    if (!CreateBlocks())  retval = false;   // Creates the command blocks
    PopulateBlocks();                       // Populate each block queue
  }
  else retval = false;                      // Failed to open file

  return retval;
}
//----------------------------------------------------------------------
// Reference to name list of all blocks
//----------------------------------------------------------------------
const clparser::strvec &clparser::Blocks() const
{
  return blknames;
}
//----------------------------------------------------------------------
// Reference to each queue
//----------------------------------------------------------------------
clparser::strqueue &clparser::Queue(csr BlockName)
{
//  return blks[Func2Block(BlockName)];
  return blks[BlockName];
}
//----------------------------------------------------------------------
// Shows parsing errors in OutStream
//----------------------------------------------------------------------
void clparser::ParsingErrors(std::ostream &OutStream)
{
  for (auto &err_msg : prserr)
    OutStream << "Line " << err_msg.first << ": " << err_msg.second << std::endl;
}
//----------------------------------------------------------------------
// Shows parsing messages in OutStream
//----------------------------------------------------------------------
void clparser::ParsingMessages(std::ostream &OutStream)
{
  for (auto &msg : prsmsg)
    OutStream << msg << std::endl;
}
//----------------------------------------------------------------------
// Show Messages in real time
//----------------------------------------------------------------------
void clparser::ShowParsingMessages(bool sh)
{
  shpm = sh;
}
//----------------------------------------------------------------------
// Show Errors in real time
//----------------------------------------------------------------------
void clparser::ShowParsingErrors(bool sh)
{
  shpe = sh;
}
//----------------------------------------------------------------------
// static functions
//----------------------------------------------------------------------
// Splits an arg. comma separated string into an arg. list
//----------------------------------------------------------------------
void clparser::ArgList(csr args, strvec &argl)
{
  std::string arg{args};            // Copies argument string

  size_t p = arg.find(',');         // Looks for arguments delimiter

  while (p != std::string::npos)     // Continue if ',' is found
  {
    argl.push_back(arg.substr(0,p));                // Pushes argument into queue and...
    arg = arg.substr(p + 1, arg.length() - p - 1);  // ...erases it from arg list.
    p = arg.find(',');                              // Looks for next argument delimiter
  }

  if (!arg.empty())     // There is still an argument in the list...
    argl.push_back(arg);        // ...pushes it into queue.
}
//----------------------------------------------------------------------
// Private functions
//----------------------------------------------------------------------
// For each OpenBrace there must be a CloseBrace in s. No nesting.
// Return either an empty string or a list of comma separated line
// numbers containing the offending brace.
//----------------------------------------------------------------------
bool clparser::BracesMatch(char OpenBrace, char CloseBrace)
{
  size_t ob1 = Contents.find_first_of(OpenBrace),    // Looks for first opening...
         cb1 = Contents.find_first_of(CloseBrace),   // ...and closing braces
         N = prserr.size();                     // Initial size of parsing errors container

  // Opening brace does not have a matching closing brace
  if (ob1 != std::string::npos && cb1 == std::string::npos)
    SetParseError(ErrorMessage(NO_OBR_MATCH), GetLineNumber(ob1));

  // Closing brace does not have a matching opening brace
  if (ob1 == std::string::npos && cb1 != std::string::npos)
    SetParseError(ErrorMessage(NO_CBR_MATCH), GetLineNumber(cb1));

  while ( ob1 != std::string::npos && cb1 != std::string::npos )
  {
    // Checks that open/close braces are in order
    if (cb1 < ob1)
      SetParseError(ErrorMessage(REV_BR), GetLineNumber(cb1));

    size_t ob2 = Contents.find_first_of(OpenBrace, ob1 + 1),   // Next opening brace
           cb2 = Contents.find_first_of(CloseBrace, cb1 + 1);  // Next closing brace

    // Opening brace does not have a matching closing brace
    if (ob2 != std::string::npos && cb2 == std::string::npos)
      SetParseError(ErrorMessage(NO_OBR_MATCH), GetLineNumber(ob2));

    // Closing brace does not have a matching opening brace
    if (ob2 == std::string::npos && cb2 != std::string::npos)
      SetParseError(ErrorMessage(NO_CBR_MATCH), GetLineNumber(cb2));

    // New brace opened before previous was closed
    if (ob2 < cb1)
      SetParseError(ErrorMessage(NESTED_BR), GetLineNumber(ob2));

    ob1 = ob2;
    cb1 = cb2;
  }

  N = prserr.size() - N;     // Number of characters added to parsing message
  return (N == 0);
}
//----------------------------------------------------------------------
// Checks that blocks { } have proper headings
//----------------------------------------------------------------------
bool clparser::CheckBlocks()
{
  size_t p1 = Contents.find_first_of('{'),  // Looks for first {
         N = prserr.size();                 // Stores initial size of error container

  while (p1 != std::string::npos)
  {
    if ( p1 == 0 ||                   // Found { at first character
        Contents[p1 - 1] != ')')      // Preceeding character must be )
      SetParseError(ErrorMessage(NO_BL_HEAD), GetLineNumber(p1));   // Set error message

    p1 = Contents.find_first_of('{', p1 + 1);    // Looks for next )
  }

  N = prserr.size() - N;    // Number of lines added to parsing message
  return (N == 0);
}
//----------------------------------------------------------------------
// Checks whether every ) is followed by a either semicolon or a {
//----------------------------------------------------------------------
bool clparser::CheckFunctions()
{
  size_t p1 = Contents.find_first_of(')'),  // Looks for first (
         N = prserr.size();                 // Stores initial size of error container

  while (p1 != std::string::npos)
  {
    if ( p1 == Contents.length() ||     // There is nothing after ')' or...
         ( Contents[p1 + 1] != ';' &&   // character following ')' is neither ';'...
           Contents[p1 + 1] != '{' ) )  // ...nor '{'. Here it is none. So...
      SetParseError(ErrorMessage(NO_PAR_TERM), GetLineNumber(p1));  // ...set an error message

    p1 = Contents.find_first_of(')', p1 + 1);    // Looks for next )
  }

  N = prserr.size() - N;    // Number of lines added to parsing error message
  return (N == 0);
}
//----------------------------------------------------------------------
// Checks Semicolons
//----------------------------------------------------------------------
bool clparser::CheckSemiColons()
{
  size_t p1 = Contents.find_first_of(';'),  // Looks for first ';'
         N = prserr.size();                 // Stores initial size of error container

  while (p1 != std::string::npos)
  {
    if (p1 == Contents.length() ||    // There is nothing after ';' or...
        Contents[p1 + 1] == '{' )     // ...there is an opening brace.
      SetParseError(ErrorMessage(LONE_SC), GetLineNumber(p1));  // ...set an error message

    if ( p1 == 0 ||                   // Found ; at first character
        Contents[p1 - 1] != ')')      // Preceeding character must be )
      SetParseError(ErrorMessage(LONE_SC), GetLineNumber(p1));   // Set error message

    p1 = Contents.find_first_of(';', p1 + 1);    // Looks for next )
  }

  N = prserr.size() - N;        // Number of lines added to parsing error message
  return (N == 0);
}
//----------------------------------------------------------------------
// Checks whether Contents respects the syntax of a command line
//----------------------------------------------------------------------
bool clparser::CheckSyntax()
{
  bool retval = true;
  std::string msg;

  if ( BracesMatch('{', '}') )
    SetParseMessage("Brackets match [OK]");
  else retval = false;

  if ( BracesMatch('(', ')') )
    SetParseMessage("Parenthesis match [OK]");
  else retval = false;

  if ( CheckBlocks() )
    SetParseMessage("Blocks [OK]");
  else retval = false;

  if ( CheckFunctions() )
    SetParseMessage("Functions calls [OK]");
  else retval = false;

  if ( CheckSemiColons() )
    SetParseMessage("Semicolons [OK]");
  else retval = false;

  return retval;
}
//----------------------------------------------------------------------
// Creates entries for each block in the blocks map
// Blocks are delimited by command between ";" or "}" and "{"
//----------------------------------------------------------------------
bool clparser::CreateBlocks()
{
  bool problem = false;

  blks["root"] = clparser::strqueue();    // Creates root block
  blknames.push_back("root()");

  size_t p2 = Contents.find_first_of('{'),    // Finds first occurence of {...
         p1 = Contents.find_last_of(";}",p2); // ...and previous termination.

  p1++;  // Handles case where p1 = npos and makes it 0 (no previous termination)

  while (p2 != std::string::npos)
  {
    std::string s1 = Contents.substr(p1, p2 - p1);  // Creates block name
//    blknames.push_back(Block2Func(s1));                         // Populate block names list
    blknames.push_back(s1);                         // Populate block names list

    if (blks.find(s1) == blks.end())      // Checks if block is not already present...
    {
      size_t blksize = Contents.find_first_of('}', p1) - p1 + 1;
      blks[s1] = clparser::strqueue();    // ...and either adds a new block...
      blk_st[s1] = {p1,blksize};     // Stores the block position and size
    }
    else                                  // ...or shows an error message.
    {
//      SetParseError(ErrorMessage(DUPL_BLK) + Block2Func(s1), GetLineNumber(p1));
      SetParseError(ErrorMessage(DUPL_BLK) + s1, GetLineNumber(p1));
      problem = true;
    }

    p2 = Contents.find_first_of('{', p2 + 1);   // Next block
    p1 = Contents.find_last_of(";}",p2) + 1;    // Next block name definition
  }

  return !problem;
}
//----------------------------------------------------------------------
// Gets string from error code
//----------------------------------------------------------------------
std::string clparser::ErrorMessage(unsigned e)
{
  switch (e)
  {
    case NO_OBR_MATCH:    return "Opening brace does not have a matching closing brace.";
    case NO_CBR_MATCH:    return "Closing brace does not have a matching opening brace.";
    case REV_BR:          return "Opening and closing braces order is reversed.";
    case NESTED_BR:       return "Brace is opened without closing previous block.";
    case NO_BL_HEAD:      return "Headless block.";
    case NO_PAR_TERM:     return "Parenthesis not properly terminated ( missing ';' or '{' ).";
    case LONE_SC:         return "Misplaced ';' .";
    case NO_QUOT_MATCH:   return "Non matching quotes.";
    case NO_FILE:         return "Couldn't open ";
    case DUPL_BLK:        return "Duplicate block ";
    default:              return "Unknown error";
  }
}
//----------------------------------------------------------------------
// Return the line number that contains position p in Contents
//----------------------------------------------------------------------
size_t clparser::GetLineNumber(size_t p)
{
  if    (ln.empty()) return 0;  // Nothing mapped
  auto  i = ln.begin();         // Get first element
  while (p > i->first)  i++;    // Increases iterator until position in contents is past (or at p)
  if    (p != i->first) i--;    // If position at contents is not exactly at p, decreases iterator
  return i->second;
}
//----------------------------------------------------------------------
// Load queue with block contents
//----------------------------------------------------------------------
void clparser::LoadQueue(csr name, csr cmds)
{
  auto &q = blks[name];   // Reference to queue attached to name

  std::string remaining_cmds(cmds);       // Copy of cmds
  size_t p1 = remaining_cmds.find(";");   // Looks for first ';'

  while (p1 != std::string::npos)         // Continues if ';' are found
  {
    std::string one_cmd = remaining_cmds.substr(0, p1);   // Extracts command and...
    remaining_cmds = remaining_cmds.substr(p1 + 1);       // ...erases it from command list.

    size_t p2 = one_cmd.find('(');        // Looks for argument list delimiter
    q.push(one_cmd.substr(0,p2));         // Pushes command into queue
    q.push(one_cmd.substr(p2 + 1, one_cmd.length() - p2 - 2));  // Pushes argument into queue
    p1 = remaining_cmds.find(";");        // Looks for next command delimiter
  }
}
//----------------------------------------------------------------------
// Populates each block queue
//----------------------------------------------------------------------
void clparser::PopulateBlocks()
{
  std::string root(Contents);   // String to hold root commands

   // Goes trough all block starts
  for (auto &b : blk_st)
  {
    size_t p0 = b.second.first,         // Beginning of block
           p1 = Contents.find('{', p0), // Block command area
           p2 = p0 + b.second.second;   // Block size

    std::string s = Contents.substr(p1 + 1, p2 - p1 - 2); // Block contents
    LoadQueue(b.first, s);                                // Load commands into block queue

    // Erases all portions of Contents that do not pertain to the root
    for (size_t i = p0; i < p2; i++)
        root[i] = ' ';
  }

  root.erase( std::remove_if(root.begin(), root.end(), ::isspace), root.end() );    // Remove spaces
  LoadQueue("root", root);           // Load commands into root queue
}
//----------------------------------------------------------------------
// Reads the configuration file
// The file contents go into a string and locations in the string are
// stored in a map associating them with line numbers in the file.
//----------------------------------------------------------------------
bool clparser::ReadFile(csr FileName)
{
  Contents.clear();                   // Clear string that hold file contents
  std::ifstream FileIn(FileName);     // Tries to open the file

  if (FileIn.fail())                  // File opening failed
  {
    SetParseError(ErrorMessage(NO_FILE) + FileName);  // Error message
    return false;                                     // Aborts
  }

  SetParseMessage("Reading " + FileName);     // Success

  size_t LineCount = 0;

  while (!FileIn.eof())             // Goes through the whole file
  {
    std::string r;
    std::getline(FileIn, r);        // Reads one line at a time
    LineCount++;                    // Increments file line number

    if (!r.empty() && r[0] != '#')  // Line read is not empty and is not a comment
    {
      // Erases comment part of lines that have also commands
      size_t p = r.find_first_of('#');    // Looks for first occurrence of '#'...
      if (p != std::string::npos)         // ...and if it is found...
        r.erase(p);                       // ...erases from the point to the end of the line.

      // Checks line and removes all that is useless
      if (!CleanCheckString(r,NoCS))
        SetParseError(ErrorMessage(NO_QUOT_MATCH), LineCount);

      if (!r.empty())
      {
        if (Contents.back() == ';' && r[0] == ';')  // Checks for consecutive ; in different lines
          r.erase(r.begin());                       // Erase first character (;) ot second string

        ln[Contents.length()] = LineCount;  // New line from file will start at position past the end of the string
        Contents += r;                      // Add line from file to string
      }
    }
  }

  SetParseMessage("Read " + std::to_string(LineCount) + " lines [OK]");

  return true;
}
//----------------------------------------------------------------------
// Copy string to parsing errors
//----------------------------------------------------------------------
void clparser::SetParseError(csr msg, size_t line)
{
  prserr.insert(std::pair<size_t,std::string>(line,msg));
  if (shpe)
    os << "Line " << line << " :" << msg << std::endl;
}
//----------------------------------------------------------------------
// Copy string to parsing messages
//----------------------------------------------------------------------
void clparser::SetParseMessage(csr msg)
{
  prsmsg.push_back(msg);
  if (shpm)
    os << msg << std::endl;
}




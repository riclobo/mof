//----------------------------------------------------------------------
// c-like scripting language parser
// This is a minimal parser for a loosely c-like scripting language.
//
// The basic elements of the scripting file:
//    * All commands have parenthesis, either empty or with a
//      parameter list;
//    * Commands are terminated by a semicolon (;);
//    * Commands can appear either alone or within a block;
//    * Blocks are delimited by { and };
//    * There is no support for nested blocks;
//    * A block should have a name in the form of a command;
//    * Multiple blocks with the same name should be differentiated
//      by a parameter to the block name command;
//    * Everything from a '#' character to the end of the line is
//      ignored;
//    * 'root' is a reserved word for the queue of commands outside
//      user defined blocks.
//
// The parser checks for basic errors and duplicate blocks. After
// parsing, a series of FIFO queues are created. One queue is always
// created to contain commands outside any block. One queue is created
// for each user-defined block. Queues are created independently.
//----------------------------------------------------------------------
#ifndef CLPARSER_H
#define CLPARSER_H
//----------------------------------------------------------------------
#include <iostream>
#include <map>
#include <queue>
#include <string>
#include <vector>
//----------------------------------------------------------------------
class clparser
{
  public:
    // Definitions
    typedef std::map<size_t,size_t>                   intmap;
    typedef std::map< std::string,
                      std::pair<size_t,size_t> >      siimap;
    typedef std::multimap<size_t,std::string>         intstrmmap;
    typedef std::queue<std::string>                   strqueue;
    typedef std::vector<std::string>                  strvec;
    typedef std::map<std::string,strqueue>            blockmap;
    typedef const std::string &                       csr;

    // Constructor
    clparser(std::ostream &OutStream,       // Stream out for error messages
             bool CaseSensitive = true)     // Should commands be case sensitive ?
      : NoCS(!CaseSensitive), os(OutStream) { }

    // Destructor
    ~clparser() { }

    // Parse a file
    bool Parse(csr FileName);

    const strvec  &Blocks() const;        // Reference to name list of all blocks
    strqueue      &Queue(csr BlockName);  // Reference to each queue

    // Returns parsing messages & errors
    void ParsingMessages(std::ostream &OutStream);
    void ParsingErrors(std::ostream &OutStream);

    // Set real time message showing flags
    void ShowParsingMessages(bool sh = true);
    void ShowParsingErrors(bool sh = true);

    // static functions
    static void ArgList(csr args, strvec &argl);  // Splits an arg. comma separated string into an arg. list

  private:
    enum        // Enumeration for error codes
    {
      NO_OBR_MATCH = 1,   // Opening brace does not have a matching closing brace.
      NO_CBR_MATCH,       // Closing brace does not have a matching opening brace.
      REV_BR,             // Opening and closing braces order is reversed.
      NESTED_BR,          // Brace is opened without closing previous block.
      NO_BL_HEAD,         // Headless block.
      NO_PAR_TERM,        // Parenthesis not properly terminated ( missing ';' or '{' ).
      LONE_SC,            // Misplaced ';' .
      NO_QUOT_MATCH,      // Non matching quotes.
      NO_FILE,            // Couldn't open <filename>
      DUPL_BLK            // Duplicate block
    };

    // Private functions
    bool        BracesMatch(char OpenBrace, char CloseBrace);
    bool        CheckBlocks();
    bool        CheckFunctions();
    bool        CheckSemiColons();
    bool        CheckSyntax();
    bool        CreateBlocks();
    std::string ErrorMessage(unsigned e);
    size_t      GetLineNumber(size_t p);
    void        LoadQueue(csr name, csr cmds);
    void        PopulateBlocks();
    bool        ReadFile(csr FileName);
    void        SetParseError(csr msg, size_t line = 0);
    void        SetParseMessage(csr msg);

    bool    NoCS,           // True means not case sensitive. Converts commands and non quoted parameters to lowercase
            shpm{false},    // Show parsing messages in std::cout as they appear
            shpe{false};    // Show parsing errors in std::cout as they appear

    blockmap    blks;       // Map to FIFO queues, one per block (plus root)
    siimap      blk_st;     // Positions and sizes of each block start in Content string

    intmap      ln;         // Line numbers -- string positions to file ln's

    intstrmmap  prserr;     // Multimap with parsing errors
    strvec      prsmsg,     // Messages from parsing file
                blknames;   // Names of all blocks

    std::ostream &os;

    std::string Contents;   // File contents
};
#endif // CLPARSER_Hcd





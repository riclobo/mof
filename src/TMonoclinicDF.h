//----------------------------------------------------------------------------------------
// File created: 2009/02/08
// Last modified: 2009/02/08
//----------------------------------------------------------------------------------------
#ifndef TMonoclinicDFH
#define TMonoclinicDFH
//----------------------------------------------------------------------------------------
// Definitions for index of monoclinic Lorentz oscillator parameters
//----------------------------------------------------------------------------------------
#define MLFreq 0
#define MLStrength 1
#define MLGamma 2
#define MLAlpha 3
//----------------------------------------------------------------------------------------
#include <vector>
#include <istream>
#include <complex>
//----------------------------------------------------------------------------------------
typedef const std::vector<double> Vec_I_DBL;
typedef std::vector<double> Vec_DBL, Vec_O_DBL, Vec_IO_DBL;
typedef std::complex<double> COMPLEX;    // Short definition for complex numbers
//----------------------------------------------------------------------------------------
// Monoclinic Lorentz oscillator class
//----------------------------------------------------------------------------------------
class TMonoclinicLorentz
{
	public:
		TMonoclinicLorentz();															// Constructor
		TMonoclinicLorentz(const TMonoclinicLorentz &ML);	// Copy constructor
		TMonoclinicLorentz(double W,											// Constructor with frequency...
											 double dE,                     // ...oscillator strenght...
											 double g,                      // ...damping...
											 double alpha);                 // ...and angle.

		~TMonoclinicLorentz() {};														// Destructor

		inline bool AutoFit(int param)          		// Get state of autofit flag for 'param'
			{ return af[param]; }

		inline void AutoFit(int param, bool state)	// Set state of autofit flag for 'param'
			{ af[param] = state; }

		inline double &operator[](int param)   			// Reference to a parameter
			{ return p[param]; }

		inline const double &operator[](int param) const // const version
			{ return p[param]; }

		COMPLEX ChiXX(double w) const;   							// Dielectric susceptibility : XX component
		COMPLEX ChiXY(double w) const;							// Dielectric susceptibility : XY component
		COMPLEX ChiYY(double w) const;							// Dielectric susceptibility : YY component

		const TMonoclinicLorentz &operator=(const TMonoclinicLorentz &ML); // Operator =

		// Friend operators
		friend bool operator==(const TMonoclinicLorentz &lML, const TMonoclinicLorentz &rML);
		friend bool operator<(const TMonoclinicLorentz &lML, const TMonoclinicLorentz &rML);

		// Streams
		friend std::istream& operator>>(std::istream &DataIn, TMonoclinicLorentz &ML);
		friend std::ostream &operator<<(std::ostream &DataOut, const TMonoclinicLorentz &ML);

	private:
		bool af[4];						// Tells which parameters to vary in least squares fit
		double p[4];         	// Parameters in monoclinic Lorentz oscillator
};
//----------------------------------------------------------------------------------------
// Monoclinic Lorentz dielectric function class
//----------------------------------------------------------------------------------------
class TMonoclinicDF
{
	public:
		TMonoclinicDF();														// Constructor
		TMonoclinicDF(const TMonoclinicDF &MDF);		// Copy constructor
		~TMonoclinicDF() {}													// Destructor

		void fromVector(Vec_I_DBL a) ;					// Load a parameters vector
		void writeVector(Vec_O_DBL &a);								// write parameters to vector

		void Add(const TMonoclinicLorentz &ML);			// Add an oscillator
		void Clear();																// Clears the dielectric function
		unsigned int NumberOfPars();             		// Number of parameters in DF
		void Remove(unsigned int Index);						// Remove an oscillator

		inline unsigned int Size() const						// Return container size
			{ return Osc.size(); }

		void Sort();                								// Sort the oscillator list

		COMPLEX EpsXX(double W) const;      					// XX component of dielectric funtion at frequency W
		COMPLEX EpsXY(double W) const;      					// XY component of dielectric funtion at frequency W
		COMPLEX EpsYY(double W) const;      					// YY component of dielectric funtion at frequency W

		COMPLEX atanC(COMPLEX a) const;								// Complex atan

		double R(double W, double Alpha) const;						//Calculate the reflectivity for polarized light making an angle Alpha with X-axis

		inline double EiXX() const { return Einf_xx; }       	// Get Einf_xx
		inline void EiXX(double ei) { Einf_xx = ei; }					// Set Einf_xx
		inline bool AutoEiXX() const { return afEinf_xx; }    // Get autofit state of Einf_xx
		inline void AutoEiXX(bool b) { afEinf_xx = b; }				// Set autofit state of Einf_xx

		inline double EiYY() const { return Einf_yy; }       	// Get Einf_yy
		inline void EiYY(double ei) { Einf_yy = ei; }					// Set Einf_yy
		inline bool AutoEiYY() const { return afEinf_yy; }    // Get autofit state of Einf_yy
		inline void AutoEiYY(bool b) { afEinf_yy = b; }				// Set autofit state of Einf_yy

		inline double EiXY() const { return Einf_xy; }       	// Get Einf_xy
		inline void EiXY(double ei) { Einf_xy = ei; }					// Set Einf_xy
		inline bool AutoEiXY() const { return afEinf_xy; }    // Get autofit state of Einf_xy
		inline void AutoEiXY(bool b) { afEinf_xy = b; }				// Set autofit state of Einf_xy

		TMonoclinicLorentz &operator[](unsigned int Index)              // Retrieve i-th osc.
			{ return Osc[Index]; }
		const TMonoclinicLorentz &operator[](unsigned int Index) const 	// Retrieve i-th osc.
			{ return Osc[Index]; }

		const TMonoclinicDF &operator=(const TMonoclinicDF &MDF);

		// Streams
		friend std::ostream& operator<<(std::ostream &dataout, const TMonoclinicDF &MDF);
		friend std::istream& operator>>(std::istream &datain, TMonoclinicDF &MDF);

	private:
		typedef std::vector<TMonoclinicLorentz>::iterator Iter;
		typedef std::vector<TMonoclinicLorentz>::const_iterator cteIter;

		bool afEinf_xx, afEinf_yy, afEinf_xy;				// Autofit state for HF DF components
		double Einf_xx, Einf_yy, Einf_xy;						// HF Components of the dielectric tensor

		std::vector<TMonoclinicLorentz> Osc;				// Monoclinic Lorentz oscillators
};


#endif

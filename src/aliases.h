//----------------------------------------------------------------------
// Aliases for numerical constants
//----------------------------------------------------------------------
#ifndef ALIASES_H
#define ALIASES_H
//----------------------------------------------------------------------
namespace al
{
  // All purpose
  const unsigned nothing = 0x0000;

  // Optical Functions
  const unsigned eps1    = 0x01;
  const unsigned eps2    = 0x02;
  const unsigned sig1    = 0x04;
  const unsigned sig2    = 0x08;
  const unsigned allof   = eps1  | eps2  | sig1 | sig2;
  const unsigned loss    = 0x10;
  const unsigned R       = 0x20;

  // Matrix elements
  const unsigned XX    = 0x01;
  const unsigned YY    = 0x02;
  const unsigned XY    = 0x04;
  const unsigned allme = XX | YY | XY;
}
//----------------------------------------------------------------------
#endif // ALIASES_H

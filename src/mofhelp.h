//----------------------------------------------------------------------
// Help for mof program. Answers to mof -h [command]
//----------------------------------------------------------------------
#ifndef MOF_HELP_H
#define MOF_HELP_H
//----------------------------------------------------------------------
#include <string>
//----------------------------------------------------------------------
void mof_general_help();
void mof_command_help(const std::string &comm);
void PrintListOfTopics();
void HelpFile();
void HelpDF();
void HelpLimits();
void HelpMatElem();
void HelpOptFunc();
void HelpOuput();
void HelpRefl();
void HelpLoss();

#endif // MOF_HELP_H

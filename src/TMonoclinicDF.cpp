//----------------------------------------------------------------------------------------
// File created: 2009/02/08
// Last modified: 2009/02/08
//----------------------------------------------------------------------------------------
#include <algorithm>
//----------------------------------------------------------------------------------------
#include "TMonoclinicDF.h"
//----------------------------------------------------------------------------------------
#define ToRad 0.01745329251   // pi / 180
//----------------------------------------------------------------------------------------
// Monoclinic Lorentz - Constructor
//----------------------------------------------------------------------------------------
TMonoclinicLorentz::TMonoclinicLorentz()
{
	for (int i = 0; i < 4; i++)
	{
		p[i] = 0;
		af[i] = true;
	}
}
//----------------------------------------------------------------------------------------
// Monoclinic Lorentz - Copy constructor
//----------------------------------------------------------------------------------------
TMonoclinicLorentz::TMonoclinicLorentz(const TMonoclinicLorentz &ML)
{
	for (int i = 0; i < 4; i++)
	{
		p[i] = ML.p[i];
		af[i] = ML.af[i];
	}
}
//----------------------------------------------------------------------------------------
// Monoclinic Lorentz - Constructor with frequency, osc. strenght, damping, and angle.
//----------------------------------------------------------------------------------------
TMonoclinicLorentz::TMonoclinicLorentz(double W, double dE, double g, double alpha)
{
	p[MLFreq] = W;
	p[MLStrength] = dE;
	p[MLGamma] = g;
	p[MLAlpha] = alpha;
	for (int i = 0; i < 4; i++)
		af[i] = true;
}
//----------------------------------------------------------------------------------------
// XX component of dielectric susceptibility for a monoclinic Lorentz oscillator
//----------------------------------------------------------------------------------------
COMPLEX TMonoclinicLorentz::ChiXX(double w) const
{
	double DEps = p[MLStrength];

	if (fabs(DEps) < 1.0E-7)
		return 0.0;

	double WT2 = p[MLFreq] * p[MLFreq],
	       CosAng = cos(p[MLAlpha] * ToRad);

	return ( CosAng * CosAng *DEps * WT2 / COMPLEX(WT2 - w * w, -p[MLGamma] * w) );
}
//----------------------------------------------------------------------------------------
// XY component of dielectric susceptibility for a monoclinic Lorentz oscillator
//----------------------------------------------------------------------------------------
COMPLEX TMonoclinicLorentz::ChiXY(double w) const
{
	double DEps = p[MLStrength];

	if (fabs(DEps) < 1.0E-7)
		return 0.0;

	double WT2 = p[MLFreq] * p[MLFreq],
	       Ang = p[MLAlpha] * ToRad;
	return ( cos(Ang)*sin(Ang)*DEps * WT2 / COMPLEX(WT2 - w * w, -p[MLGamma] * w) );
}
//----------------------------------------------------------------------------------------
// YY component of dielectric susceptibility for a monoclinic Lorentz oscillator
//----------------------------------------------------------------------------------------
COMPLEX TMonoclinicLorentz::ChiYY(double w) const
{
	double DEps = p[MLStrength];

	if (fabs(DEps) < 1.0E-7)
		return 0.0;

	double WT2 = p[MLFreq] * p[MLFreq],
	       SinAng = sin(p[MLAlpha] * ToRad);

	return ( SinAng * SinAng * DEps * WT2 / COMPLEX(WT2 - w * w, -p[MLGamma] * w) );
}
//----------------------------------------------------------------------------------------
// Monoclinic Lorentz Operator =
//----------------------------------------------------------------------------------------
const TMonoclinicLorentz &TMonoclinicLorentz::operator=(const TMonoclinicLorentz &ML)
{
	if (this == &ML)
		return (*this);

	for (int i = 0; i < 4; i++)
		p[i] = ML.p[i];

	return (*this);
}
//----------------------------------------------------------------------------------------
// (friend) Monoclinic Lorentz Operator ==
//----------------------------------------------------------------------------------------
bool operator==(const TMonoclinicLorentz &lML, const TMonoclinicLorentz &rML)
{
	// Check each parameter
	for (int i = 0; i < 4; i++)
		if (lML.p[i] != rML.p[i])
			return false;

	// Everything is the same
	return true;
}
//----------------------------------------------------------------------------------------
// (friend) Monoclinic Lorentz Operator < - The comparison is done only for the frequency
//----------------------------------------------------------------------------------------
bool operator<(const TMonoclinicLorentz &lML, const TMonoclinicLorentz &rML)
{
	return (lML.p[0] < rML.p[0]);
}
//----------------------------------------------------------------------------------------
// (friend) Monoclinic Lorentz stream in
//----------------------------------------------------------------------------------------
std::istream& operator>>(std::istream &DataIn, TMonoclinicLorentz &ML)
{
	for (int i = 0; i < 4; i++)
		DataIn >> ML.p[i];              // Read parameters
	return DataIn;
}
//----------------------------------------------------------------------------------------
// (friend) Monoclinic Lorentz stream out
//----------------------------------------------------------------------------------------
std::ostream &operator<<(std::ostream &DataOut, const TMonoclinicLorentz &ML)
{
	DataOut.precision(15);            // Maximum double precision
	DataOut << ML.p[0] << '\t' << ML.p[1] << '\t' << ML.p[2] << '\t' << ML.p[3];
	return DataOut;
}

//----------------------------------------------------------------------------------------
// Monoclinic Lorentz dielectric function - Constructor
//----------------------------------------------------------------------------------------
TMonoclinicDF::TMonoclinicDF()
{
	Clear();
}
//----------------------------------------------------------------------------------------
// Monoclinic Lorentz dielectric function - Copy constructor
//----------------------------------------------------------------------------------------
TMonoclinicDF::TMonoclinicDF(const TMonoclinicDF &MDF)
{
	(*this) = MDF;
}
//----------------------------------------------------------------------------------------
// Monoclinic Lorentz dielectric function - Add an oscillator
//----------------------------------------------------------------------------------------
void TMonoclinicDF::Add(const TMonoclinicLorentz &ML)
{
	Osc.push_back(ML);
}
//----------------------------------------------------------------------------------------
// Monoclinic Lorentz dielectric function - Clears the dielectric function
//----------------------------------------------------------------------------------------
void TMonoclinicDF::Clear()
{
	Einf_xx = Einf_yy = 1.0;
	afEinf_xx = afEinf_yy = true;

	Einf_xy = 0.0;
	afEinf_xy = true;

	Osc.clear();
}
//----------------------------------------------------------------------------------------
// Monoclinic Lorentz dielectric function - Number of parameters in DF
//----------------------------------------------------------------------------------------
unsigned int TMonoclinicDF::NumberOfPars()
{
	return 3 + Size() * 4;			// 3 values for epsilon infinity and 4 per oscillator
}
//----------------------------------------------------------------------------------------
// Monoclinic Lorentz dielectric function - Removes an oscillator
//----------------------------------------------------------------------------------------
void TMonoclinicDF::Remove(unsigned int Index)
{
	Osc.erase(Osc.begin() + Index);
}
//----------------------------------------------------------------------------------------
// Monoclinic Lorentz dielectric function - Sort the oscillator list
//----------------------------------------------------------------------------------------
void TMonoclinicDF::Sort()
{
	std::sort(Osc.begin(), Osc.end());
}
//----------------------------------------------------------------------------------------
// Calculate the XX component of dielectric funtion at frequency w
//----------------------------------------------------------------------------------------
COMPLEX TMonoclinicDF::EpsXX(double W) const
{
	COMPLEX epsxx = Einf_xx;
	for(size_t i = 0; i <  Osc.size(); i++)
	{
		epsxx += Osc[i].ChiXX(W);
	}
	return epsxx;
}
//----------------------------------------------------------------------------------------
// Calculate the XY component of dielectric funtion at frequency w
//----------------------------------------------------------------------------------------
COMPLEX TMonoclinicDF::EpsXY(double W) const
{
	COMPLEX epsxy = Einf_xy;
	for(size_t i = 0; i < Osc.size(); i++)
	{
		epsxy += Osc[i].ChiXY(W);
	}
	return epsxy;
}
//----------------------------------------------------------------------------------------
// Calculate the YY component of dielectric funtion at frequency w
//----------------------------------------------------------------------------------------
COMPLEX TMonoclinicDF::EpsYY(double W) const
{
	COMPLEX epsyy = Einf_yy;
	for(size_t i = 0; i < Osc.size(); i++)
	{
		epsyy += Osc[i].ChiYY(W);
	}
	return epsyy;
}
//----------------------------------------------------------------------------------------
// Complex atan
//----------------------------------------------------------------------------------------
COMPLEX TMonoclinicDF::atanC(COMPLEX c) const
{
	double real = c.real();
	double imag = c.imag();
	COMPLEX log_v = log( COMPLEX( 1 - imag, real ) / COMPLEX( 1 + imag, -real ) );

return COMPLEX( log_v.imag() / 2.0, - log_v.real() /2.0);
}
//----------------------------------------------------------------------------------------
// Calculate the reflectivity at frequency W, with light polarized along Alpha (degrees)
//----------------------------------------------------------------------------------------
double TMonoclinicDF::R(double W, double Alpha) const
{
	//conversion de Alpha en radians
	Alpha *= ToRad ;

	COMPLEX eps_xx = EpsXX(W),
	        eps_xy = EpsXY(W),
	        eps_yy = EpsYY(W);

	COMPLEX xmy2 = eps_xx - eps_yy;
	xmy2 *= xmy2;

	COMPLEX sumxy = eps_xx + eps_yy,
	        xy2 = eps_xy * eps_xy;

//	n_u2 = 0.5*(eps_xx + eps_yy + sqrt(pow((eps_xx-eps_yy),2.0)+4.0*eps_xy*eps_xy));
//	n_u=sqrt(n_u2);
//	n_v2 = 0.5*(eps_xx + eps_yy - sqrt(pow((eps_xx-eps_yy),2.0)+4.0*eps_xy*eps_xy));
//	n_v=sqrt(n_v2);
	COMPLEX n_u2 = 0.5 * ( sumxy + sqrt(xmy2 + 4.0 * xy2) );
	COMPLEX n_u = sqrt(n_u2);
	COMPLEX n_v2 = 0.5 * ( sumxy - sqrt(xmy2 + 4.0 * xy2) );
	COMPLEX n_v = sqrt(n_v2);

  COMPLEX phi;
	if(eps_xy == 0.0)
		phi = atanC( (n_u2-eps_xx) / 1e-10 );
	else
		phi = atanC( (n_u2-eps_xx) / eps_xy );

	COMPLEX r_u = (1.0 - n_u) / (1.0 + n_u);
	COMPLEX r_v = (1.0 - n_v) / (1.0 + n_v);
	COMPLEX arg = 2.0 * (phi- Alpha),
	        difr = (r_u - r_v) / 2.0;

//	return   norm( (r_u + r_v) / 2.0 + (r_u - r_v) / 2.0 * cos(2.0*(phi-Alpha)) )
//	       + norm( (r_u - r_v) / 2.0 * sin(2.0*(phi-Alpha)) );
	return   norm( (r_u + r_v) / 2.0 + difr * cos(arg) )
	       + norm( difr * sin(arg) );
}


void TMonoclinicDF::fromVector(Vec_I_DBL a)
{
	Einf_xx = a[4];
	Einf_yy = a[5];
	Einf_xy = a[6];
	Osc.resize((a.size()-7)/4);
	for(unsigned int i=0; i<Osc.size(); i++)
	{
		for(int j=0; j<4; j++)
			Osc[i][j] = a[7+4*i+j];
	}
}

void TMonoclinicDF::writeVector(Vec_IO_DBL &a)
{
	a.resize(NumberOfPars()+4);
	a[4] = Einf_xx;
	a[5] = Einf_yy;
	a[6] = Einf_xy;
	for(unsigned int i=0; i<Osc.size(); i++)
	{
		for(int j=0; j<4; j++)
			a[7+4*i+j] = Osc[i][j];
	}
}


//----------------------------------------------------------------------------------------
// Monoclinic Lorentz dielectric function - Operator =
//----------------------------------------------------------------------------------------
const TMonoclinicDF &TMonoclinicDF::operator=(const TMonoclinicDF &MDF)
{
	if (this == &MDF)
		return (*this);

	Clear();                          // Clear the dielectric function

	// Copy HF dielectric constant components
	Einf_xx = MDF.Einf_xx;
	Einf_yy = MDF.Einf_yy;
	Einf_xy = MDF.Einf_xy;

	// autofit state of HF dielectric function components
	afEinf_xx = MDF.afEinf_xx;
	afEinf_yy = MDF.afEinf_yy;
	afEinf_xy = MDF.afEinf_xy;

	// Copies the oscillator vector
	Osc.clear();
	Osc = std::vector<TMonoclinicLorentz>(MDF.Osc);

  return (*this);
}
//----------------------------------------------------------------------------------------
// Monoclinic Lorentz dielectric function - Stream out
//----------------------------------------------------------------------------------------
std::ostream& operator<<(std::ostream &dataout, const TMonoclinicDF &MDF)
{
	dataout.precision(15);                          			// Max double precision
	dataout << MDF.Einf_xx << '\t' << MDF.Einf_yy << '\t'
					<< MDF.Einf_xy << std::endl           				// E_inf <<
					<< MDF.Osc.size() << std::endl;             	// ...# of oscillators

	for (unsigned int i = 0; i < MDF.Osc.size(); i++)
		dataout << MDF.Osc[i] << std::endl;               	// Sends out each oscillator

  return dataout;
}
//----------------------------------------------------------------------------------------
// Monoclinic Lorentz dielectric function - Stream in
//----------------------------------------------------------------------------------------
std::istream& operator>>(std::istream &datain, TMonoclinicDF &MDF)
{
	TMonoclinicLorentz ML;
	unsigned int n;

	datain >> MDF.Einf_xx >> MDF.Einf_yy >> MDF.Einf_xy; 		// Read Eps_inf
	datain >> n;  																					// Read # of oscillators

	for (unsigned int i = 0; i < n; i++)
  {
		datain >> ML;   						// Each oscillator gets in...
		MDF.Osc.push_back(ML);			// ...and is sent to the oscillator vector.
	}
	return datain;
}



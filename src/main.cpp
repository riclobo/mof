//----------------------------------------------------------------------
// mof - Monoclinic Optical Funcions
// (c) R.P.S.M. Lobo -- rpsml@yahoo.com
//----------------------------------------------------------------------
#include <iostream>
#include <string>
//----------------------------------------------------------------------
#include "moffuncs.h"
#include "mofhelp.h"
//----------------------------------------------------------------------
using namespace std;
//----------------------------------------------------------------------
// Main function
//----------------------------------------------------------------------
int main(int argc, char *argv[])
{
  cout << "---------------------------------" << endl
       << "Monoclinic Optical Funcions (mof)" << endl
       << "Version: 0.22"                     << endl
       << "---------------------------------" << endl;

  //
  // Parses command line arguments
  //
  if (argc != 1)      // There is at least one argument passed in addition to the program name
  {
    string arg = argv[1];

    if (arg == "-i" || arg == "--input")  // User wants to parse an input command file
    {
      if (argc == 3)          // Expects command file name
        return ParseCommandFile(argv[2]);
    }
    else if (arg == "-h" || arg == "--help")  // User needs help
    {
      if (argc == 3)    // User is calling with another parameter
        mof_command_help(argv[2]);
      else              // User just typed -h or -h with a lot of junk
        mof_general_help();

      return 0;
    }
  }

  //
  // Error parsing the command line
  //
  cout << endl;
  cout << "Usage: mof [arguments]" << endl
       << endl
       << "  -i, --input    Needs another argument: a <file> with commands"   << endl
       << "                 defining what the program should do."             << endl
       << endl
       << "  -h, --help     Prints some help about how to use the program"    << endl
       << "                 how to create a basic command file. "             << endl
       << "                 a different number."                              << endl
       << endl
       << "Only one flag can be passed at a time."                            << endl
       << endl;

  return 1;
}

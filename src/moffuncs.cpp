//----------------------------------------------------------------------
// Monoclinic optical functions
//----------------------------------------------------------------------
//#include <algorithm>
#include <chrono>
#include <iomanip>
#include <fstream>
//#include <iterator>
//----------------------------------------------------------------------
#include "clparser.h"
#include "mofdf.h"
#include "trim.h"
#include "TMonoclinicDF.h"
#include "moffuncs.h"
//----------------------------------------------------------------------
#define ToRad 0.01745329251   // pi / 180
//----------------------------------------------------------------------
using namespace std;
//----------------------------------------------------------------------
// Parses a command file into gnuplot scripts
//----------------------------------------------------------------------
int ParseCommandFile(const std::string &fn)
{
  // Start time counting
  chrono::steady_clock::time_point starttime = chrono::steady_clock::now();

  //
  // Parses the command file
  //
  clparser cp(std::cout, false);
  bool success = cp.Parse(fn);

  // Parsing messages
  cout << "** MESSAGES **" << endl;
  cp.ParsingMessages(std::cout);
  cout << endl;

  // If parsing did not succeed, shows errors
  if (!success)
  {
    cout << "** ERRORS **" << endl;
    cp.ParsingErrors(std::cout);
    return 1;
  }

  // Time milestone
  chrono::steady_clock::time_point parsetime = chrono::steady_clock::now();


  // Parses DF queues
  cout << "Parsing dielectric functions..." << endl;

  map<string,mofDF> DFs;

  for (auto &s : cp.Blocks())
  {
    size_t p = s.find("df");    // Checks if Block is a dielectric function...

    if (p != string::npos)        // ...it is!!
    {
      size_t i = s.find("(");                               // Looks for parenthesis
      std::string r = s.substr(i + 1, s.length() - i - 2);  // Looks for file name
      r = lobo::reduce(r, "", "\"");                        // Erases possible ""

      if (r.empty())        // No file name
      {
        cout << "DF() does not define a file name. Ignoring it.\n";
      }
      else
      {
        mofDF df( cp.Queue(s), r ); // Parses the dielectric function

        if (!df.Errors().empty())    // Errors messages exist
        {
          cout << "Error parsing DF(" << r << ")" << endl
               << df.Errors() << endl;
        }
        else
          DFs[r] = mofDF( df );   // No errors, adds it to map
      }
    }
    else if (s != "root()")     // This is not a dielectric function nor the root block
    {
      cout << "Warning: '" + s + "' is an unknown section type. Ignoring it...\n";
    }
  }

  // Time milestone
  chrono::steady_clock::time_point queuetime = chrono::steady_clock::now();

  //
  // Generate and save optical functions
  //
  cout << "Generating optical functions..." << endl;

  for (auto &DF : DFs)
    GenerateOptFuncs(DF.second);

  // Time milestone
  chrono::steady_clock::time_point endtime = chrono::steady_clock::now();

  // Show times
  cout << endl << setiosflags(ios::fixed) << setprecision(3)
       << "Total running time:    " << setw(12) << chrono::duration_cast<chrono::microseconds>( endtime   - starttime ).count() / 1000. << " ms" << endl
       << "Configuration parsing: " << setw(12) << chrono::duration_cast<chrono::microseconds>( parsetime - starttime ).count() / 1000. << " ms" << endl
       << "DF queues parsing:     " << setw(12) << chrono::duration_cast<chrono::microseconds>( queuetime - parsetime ).count() / 1000. << " ms" << endl
       << "DF generation:         " << setw(12) << chrono::duration_cast<chrono::microseconds>( endtime   - queuetime ).count() / 1000. << " ms" << endl
       << "And we are done..." << endl;

  return 0;
}
//----------------------------------------------------------------------
// Process a monoclinic dielectric function into optical functions
//----------------------------------------------------------------------
void GenerateOptFuncs(const mofDF &df)
{
  // Read df parameters from file
  TMonoclinicDF mdf;
  ifstream is(df.Parameters().c_str());
	if (is)                                     // ...success.
  {
	  mdf.Clear();                          	// Clear current DF
    is >> mdf;                            	// Read DF from file
    is.close();                              	// Close the file
	}
	else                                        // Could not open the file
	{
    cout << "Cannot open :" + df.Parameters() << endl;
    return;
  }

  // Sets the vector with the frequencies to calculate
  vector<double> w;
  double ww = df.First();
  while (ww < df.Last())
  {
    w.push_back(ww);
    ww += df.Step();
  }
  w.push_back(ww);

  // Calculates dielectric functions matrix
  vector<COMPLEX> exx, eyy, exy;
  for (size_t i = 0; i < w.size(); i++)
  {
    exx.push_back( mdf.EpsXX(w[i]) );
    exy.push_back( mdf.EpsXY(w[i]) );
    eyy.push_back( mdf.EpsYY(w[i]) );
  }

  // Creates a header for the data
  string Header;
  SetHeadher(Header, df);

  // Calculates the optical functions
  vector<string> Data;
  SetData(Data, w, exx, eyy, exy, df, mdf);

  // Save files -- not trying to split them for now
  string ofn = df.OutputFile() + ".dat";
  cout << "Saving " << ofn << endl;
  ofstream of(ofn.c_str());
  of << Header << endl;
  for (size_t i = 0; i < Data.size(); i++)
    of << Data[i] << endl;
  of.close();
}

//----------------------------------------------------------------------
// Creates a strring with the proper file header
//----------------------------------------------------------------------
void SetHeadher(string &Header, const mofDF &df)
{
  Header = "w";

  if (df.Elements() & al::XX)   // User wants xx matrix elements
  {
    if (df.OpticalFunctions() & al::eps1)  Header += "\tEps1_xx";
    if (df.OpticalFunctions() & al::eps2)  Header += "\tEps2_xx";
    if (df.OpticalFunctions() & al::sig1)  Header += "\tSig1_xx";
    if (df.OpticalFunctions() & al::sig2)  Header += "\tSig2_xx";
  }

  if (df.Elements() & al::YY)   // User wants yy matrix elements
  {
    if (df.OpticalFunctions() & al::eps1)  Header += "\tEps1_yy";
    if (df.OpticalFunctions() & al::eps2)  Header += "\tEps2_yy";
    if (df.OpticalFunctions() & al::sig1)  Header += "\tSig1_yy";
    if (df.OpticalFunctions() & al::sig2)  Header += "\tSig2_yy";
  }

  if (df.Elements() & al::XY)   // User wants xy matrix elements
  {
    if (df.OpticalFunctions() & al::eps1)  Header += "\tEps1_xy";
    if (df.OpticalFunctions() & al::eps2)  Header += "\tEps2_xy";
    if (df.OpticalFunctions() & al::sig1)  Header += "\tSig1_xy";
    if (df.OpticalFunctions() & al::sig2)  Header += "\tSig2_xy";
  }

  for (size_t i = 0; i < df.Angles(al::R).size(); i++)
    Header += "\tR" + std::to_string(df.Angles(al::R)[i]);

  for (size_t i = 0; i < df.Angles(al::loss).size(); i++)
    Header += "\tLoss" + std::to_string(df.Angles(al::loss)[i]);

}
//----------------------------------------------------------------------
// Calculates the functions and sets the output table
//----------------------------------------------------------------------
void SetData(vector<string> &Data, const vector<double> &w,
            const vector<COMPLEX> exx, const vector<COMPLEX> eyy,
            const vector<COMPLEX> exy, const mofDF &df,
            const TMonoclinicDF &mdf)
{
  Data.resize(w.size());

  // Calculate optical functions
  for (size_t i = 0; i < w.size(); i++)
  {

    Data[i] = std::to_string(w[i]);     // Adds the frequency to the first data column

    // Elements of the optical matrices
    COMPLEX Sig_xx,  Sig_yy,  Sig_xy;

    // Optical conductivity matrix
    if ( (df.OpticalFunctions() & al::sig1) || (df.OpticalFunctions() & al::sig2) )
    {
      Sig_xx = w[i] * (mdf.EiXX() - exx[i]) / 59.9584;
      Sig_yy = w[i] * (mdf.EiYY() - eyy[i]) / 59.9584;
      Sig_xy = w[i] * (mdf.EiXY() - exy[i]) / 59.9584;
    }

    // Adds required elements to data string
    if (df.Elements() & al::XX)
      AddToData( Data[i], exx[i], Sig_xx, df.OpticalFunctions() );

    if (df.Elements() & al::YY)
      AddToData( Data[i], eyy[i], Sig_yy, df.OpticalFunctions() );

    if (df.Elements() & al::XY)
      AddToData( Data[i], exy[i], Sig_xy, df.OpticalFunctions() );

    // Calculates the reflectivities
    for (size_t j = 0; j < df.Angles(al::R).size(); j++)
      Data[i] += '\t' + std::to_string( mdf.R(w[i], df.Angles(al::R)[j]) );

    // Calculates the loss functions
    for (size_t j = 0; j < df.Angles(al::loss).size(); j++)
    {
      double A = df.Angles(al::loss)[j] * ToRad;
      double c = cos(A),
             s = sin(A);

      COMPLEX aux = exx[i] * eyy[i] - exy[i] * exy[i];
      COMPLEX Loss_xx = -eyy[i] / aux;
      COMPLEX Loss_yy = -exx[i] / aux;
      COMPLEX Loss_xy =  exy[i] / aux;

      COMPLEX eta =   Loss_xx * c * c
                    + Loss_yy * s * s
                    + 2.0 * Loss_xy * s * c;

      Data[i] += '\t' + std::to_string( eta.imag() );
    }
  }
}
//----------------------------------------------------------------------
// Adds functions for a matrix element to the data string
//----------------------------------------------------------------------
void AddToData(string &d, const COMPLEX &e, const COMPLEX &s, unsigned of)
{
  if (of & al::eps1)  d += '\t' + std::to_string( e.real() );
  if (of & al::eps2)  d += '\t' + std::to_string( e.imag() );
  if (of & al::sig1)  d += '\t' + std::to_string( s.real() );
  if (of & al::sig2)  d += '\t' + std::to_string( s.imag() );
}

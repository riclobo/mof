#include <iostream>
#include <algorithm>
#include "mofhelp.h"

using namespace std;

void mof_general_help()
{
  cout << "mof -- General help" << endl
       << "===================" << endl
       << "Basic usage: " << endl
       << "  mof -i <file>    - Executes commands store in file" << endl
       << "  mof -h           - Shows this page" << endl
       << "  mof -h <topic>   - Shows help on a specific topic or command" << endl
       << "                     (only the first 3 letters are required)" << endl
       << endl;
  PrintListOfTopics();
}

void mof_command_help(const string &comm)
{
  string c = comm.substr(0,3);
  std::transform(c.begin(), c.end(), c.begin(), ::tolower);

  if (c == "fil")
    HelpFile();
  else if (c == "df")
    HelpDF();
  else if (c == "lim")
    HelpLimits();
  else if (c == "mat")
    HelpMatElem();
  else if (c == "opt")
    HelpOptFunc();
  else if (c == "out")
    HelpOuput();
  else if (c == "ref")
    HelpRefl();
  else if (c == "loss")
    HelpLoss();
  else
  {
    cout << "Unknown topic: " << comm << endl;
    PrintListOfTopics();
  }
}

void PrintListOfTopics()
{
  cout << "List of topics:" << endl
       << "  file            - (topic) shows the basic structure of a command file" << endl
       << "  DF              - (section head command) defines the dielectric function" << endl
       << "  Limits          - (command) defines the limits for calculations" << endl
       << "  MatrixElement   - (command) defines which matrix elements to save" << endl
       << "  OpticalFunction - (command) defines which optical functions to calculate" << endl
       << "  Output          - (command) defines the name of the output file" << endl
       << "  Reflectivity    - (command) defines angles to project the reflectivity" << endl
       << "  Loss            - (command) defines angles to project the loss function" << endl
       << endl;
}

void HelpFile()
{
  cout << endl
       << "Configuration File Structure" << endl
       << endl
       << "  A configuration file is a series of instructions" << endl
       << "  passed to mof to define what calculations are" << endl
       << "  requested. The syntax to pass the file to mof is:" << endl
       << endl
       << "    mof -i <file>" << endl
       << endl
       << "  In a configuration file, any text from the character" << endl
       << "  '#' to the end of a line is ignored." << endl
       << endl
       << "  Configuration files are a series of 'DF' sections" << endl
       << "  [see DF() section header], each DF section defining" << endl
       << "  a dielectric function to be analysed. A section is" << endl
       << "  defined by commands within a { } block." << endl
       << "  Commands are terminated by a semicolon ';'." << endl
       << "  The basic structure of a configuration file is:" << endl
       << endl
       << "    #" << endl
       << "    # Comment before the first section" << endl
       << "    #" << endl
       << "    DF(\"file1.par\")" << endl
       << "    {" << endl
       << "      command(args);" << endl
       << "      command(args);" << endl
       << "      command(args);" << endl
       << "      command(args);" << endl
       << "      command(args);" << endl
       << "    }" << endl
       << "    #" << endl
       << "    # Comment before the second section" << endl
       << "    #" << endl
       << "    DF(\"file2.par\")" << endl
       << "    {" << endl
       << "      command(args);" << endl
       << "      command(args);" << endl
       << "    }" << endl
       << endl
       << "  A minimal configuration file to calculate" << endl
       << "  the complex dielectric function for matrix" << endl
       << "  element xx and the reflecitivity at 45 degrees" << endl
       << "  from the function parameters defined in the" <<  endl
       << "  file named 'mymono.mdf' would be:" << endl
       << endl
       << "    DF(\"mymono.mdf\")" << endl
       << "    {" << endl
       << "      MatrixElement(XX);" << endl
       << "      OpticalFunction(EPS1);" << endl
       << "      OpticalFunction(EPS2);" << endl
       << "      Reflectivity(45);" << endl
       << "      Loss(60);" << endl
       << "    }" << endl
       << endl
       << "  Here, as nothing else is defined, the" << endl
       << "  calculations are done from 10 to 1000 cm-1" << endl
       << "  with a point every cm-1. The results are" << endl
       << "  saved in 'mymono.mdf.dat'" << endl
       << endl;
}

void HelpDF()
{
  cout << endl
       << "DF(\"filename\") -- Section Head" << endl
       << endl
       << "  This is a section head where" << endl
       << "  the \"filename\" defines the" << endl
       << "  file containing the monoclinic" << endl
       << "  dielectric function. " << endl
       << "  Type:" << endl
       << "    mof -h File" << endl
       << "  to see the definition of and the" << endl
       << "  usage of a section." << endl
       << endl;
}

void HelpLimits()
{
  cout << endl
       << "Limits(w0,wf,dw) -- Optional Command" << endl
       << endl
       << "  Defines the first and last frequencies" << endl
       << "  for calculations as well as the step." << endl
       << "  'First' must be smaller than 'Last'. " << endl
       << "  The program swaps the values if necessary." << endl
       << "  If this command is absent, assumes values" << endl
       << "  of w0 = 10; wf = 1000; and dw = 1." << endl
       << endl;
}

void HelpMatElem()
{
  cout << endl
       << "MatrixElement(flag) -- Optional Command" << endl
       << endl
       << "  Calculates the optical function for a" << endl
       << "  specific matrix element defined in flag." << endl
       << "  Possible flags are XX, YY, XY, ALL." << endl
       << "  Repeat the command as many times as" << endl
       << "  necessary. Doublons will be ignored." << endl
       << "  If absent, calculations default to ALL." << endl
       << endl;
}

void HelpOptFunc()
{
  cout << endl
       << "OpticalFunction(flag) -- Command" << endl
       << endl
       << "  Generates optical functions for all" << endl
       << "  matrix elements defined in flag." << endl
       << "  Possible flags are EPS1, EPS2," << endl
       << "  SIG1, SIG2, LOSS1, LOSS2" << endl
       << "  Repeat the command as many times as" << endl
       << "  necessary. Doublons will be ignored." << endl
       << "  Each section must contain at least one" << endl
       << "  of OpticalFunction(); Reflectivity(); or Loss()" << endl
       << endl;
}

void HelpOuput()
{
  cout << endl
       << "Output(\"filename\") -- Optional Command" << endl
       << endl
       << "  Defines the output file name. The extension" << endl
       << "  '.dat' will be appended to the file name." << endl
       << "  If absent uses name defined in DF()." << endl
       << endl;

}

void HelpRefl()
{
  cout << endl
       << "Reflectivity(angle) -- Command" << endl
       << endl
       << "  Calculates the reflectivity projected to" << endl
       << "  a particular angle (in degrees)." << endl
       << "  Angles are measured from the x axis, zero" << endl
       << "  being defined in the dielectric function"  << endl
       << "  phonon angles." << endl
       << "  Repeat the command for as many angles as" << endl
       << "  you want. Doublons will be ignored." << endl
       << "  Each section must contain at least one" << endl
       << "  of OpticalFunction(); Reflectivity(); or Loss()" << endl
       << endl;
}

void HelpLoss()
{
  cout << endl
       << "Loss(angle) -- Command" << endl
       << endl
       << "  Calculates the loss function projected to" << endl
       << "  a particular angle (in degrees)." << endl
       << "  Angles are measured from the x axis, zero" << endl
       << "  being defined in the dielectric function"  << endl
       << "  phonon angles." << endl
       << "  Repeat the command for as many angles as" << endl
       << "  you want. Doublons will be ignored." << endl
       << "  Each section must contain at least one" << endl
       << "  of OpticalFunction(); Reflectivity(); or Loss()" << endl
       << endl;
}


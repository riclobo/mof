//----------------------------------------------------------------------
// Class to parse a coupo queue into data for a plot
//----------------------------------------------------------------------
#include <algorithm>
#include <string>
//----------------------------------------------------------------------
#include "mofdf.h"
#include "trim.h"
//----------------------------------------------------------------------
// Constructor taking a queue<string>
//----------------------------------------------------------------------
mofDF::mofDF(clparser::strqueue &q, clparser::csr ofn) :
  OutFile(ofn), ParFile(ofn)
{
  while (!q.empty())        // Goes trough the queue
  {
    std::string s = q.front();   // Reads command and...
    q.pop();                     // ...removes it from the queue.

    if (s == "matrixelement")           // Defines the matrix elements to generate
    {
      Add_ME(lobo::reduce(q.front(), "", "\""));
      q.pop();
    }
    else if (s == "opticalfunction")    // Defines optical function to generate
    {
      Add_OF(lobo::reduce(q.front(), "", "\""));
      q.pop();
    }
    else if (s == "reflectivity")       // Defines angle to project the reflectivity - expects the angle
    {
      Add_Angle(lobo::reduce(q.front(), "", "\""), al::R);
      q.pop();
    }
    else if (s == "loss")       // Defines angle to project the loss function - expects the angle
    {
      Add_Angle(lobo::reduce(q.front(), "", "\""), al::loss);
      q.pop();
    }
    else if (s == "output")             // Defines the output file name
    {
      Def_Outfile(lobo::reduce(q.front(), "", "\""));
      q.pop();
    }
    else if (s == "limits")             // Limits for calculations of optical functions
    {
      Def_Limits(lobo::reduce(q.front(), "", "\""));
      q.pop();
    }
  }

  //
  // Clean-up
  //

  // Erase duplicate entries in reflectivity angle list
  std::sort(angs_R.begin(), angs_R.end());
  angs_R.erase(std::unique(angs_R.begin(), angs_R.end()), angs_R.end());

  // Erase duplicate entries in loss function angle list
  std::sort(angs_eta.begin(), angs_eta.end());
  angs_eta.erase(std::unique(angs_eta.begin(), angs_eta.end()), angs_eta.end());

  // Checks that at least one matrix element is defined for an optical function otherwise defaults to all
  if ( (OptFuncs != al::nothing) && (MatElements == al::nothing) )
    MatElements = al::allme;

  // Checks that at least one of Reflectivity or Optical Function is present
  if ( (OptFuncs == al::nothing) && angs_R.empty() && angs_eta.empty() )
    errors += "DF(\"" + ofn + "\"): Must have at least one of Reflectivity();\nLoss(); or OpticalFunction() commands.";

}
//----------------------------------------------------------------------
// Adds an angle to calculate the reflectivity
//----------------------------------------------------------------------
void mofDF::Add_Angle(clparser::csr s, unsigned Func)
{
  if (s.empty())     // Arguments is not an empty string: error...
  {
    errors += "Reflectivity() or Loss(): Angle is a required argument\n";
    return;
  }

  // Adds the angle (in degrees) to the list
  switch (Func)
  {
    case al::R    : angs_R.push_back(std::stod(s));
    case al::loss : angs_eta.push_back(std::stod(s));
  }
}
//----------------------------------------------------------------------
// Adds a matrix element to the list - expects xx, yy, xy or all
//----------------------------------------------------------------------
void mofDF::Add_ME(clparser::csr s)
{
  if (s.empty())     // Arguments is an empty string: error...
  {
    errors += "MatrixElement(): a matrix element is required.\n";
    return;
  }

  if      (s == "xx")  MatElements |= al::XX;
  else if (s == "yy")  MatElements |= al::YY;
  else if (s == "xy")  MatElements |= al::XY;
  else if (s == "all") MatElements |= al::allme;
  else                 errors += "MatrixElement(): accepts only one of XX, YY, XY or ALL. Got '" + s + "'\n";
}
//----------------------------------------------------------------------
// Adds an optical function to the list
// Accepted values are EPS1, EPS2, SIG1, SIG2, N, K, LOSS1, LOSS2
//----------------------------------------------------------------------
void mofDF::Add_OF(clparser::csr s)
{
  if (s.empty())     // Arguments is an empty string: error...
  {
    errors += "OpticalFunction(): a function is required.\n";
    return;
  }

  if      (s == "eps1")  OptFuncs |= al::eps1;
  else if (s == "eps2")  OptFuncs |= al::eps2;
  else if (s == "sig1")  OptFuncs |= al::sig1;
  else if (s == "sig2")  OptFuncs |= al::sig2;
  else                   errors += "OpticalFunction(): accepts only one of\n"
                                   "EPS1, EPS2, SIG1, or SIG2.\n"
                                   "Got '" + s + "'\n";
}
//----------------------------------------------------------------------
// Defines the limits for the calculations
//----------------------------------------------------------------------
void mofDF::Def_Limits(clparser::csr s)
{
  if (std::count(s.begin(), s.end(), ',') != 2)   // Wrong number of parameters. Error!
  {
    errors += "Limits(): Takes exactly 3 arguments. Got '" + s + "'\n";
    return;
  }

  size_t p1 = s.find(','),          // Looks for the first...
         p2 = s.find(',', p1 + 1);  // ...and last commas.

  w0 = std::stod(s.substr(0, p1));
  wf = std::stod(s.substr(p1 + 1, p2 - p1));
  dw = std::stod(s.substr(p2 + 1));

  // Checks that w0 and wf are ordered
  if (w0 > wf)
  {
    double aux = wf;
    wf = w0;
    w0 = aux;
  }

  // Checks that the step is positive
  if (dw <= 0.0)
  {
    errors += "Limits(): step must be positive. Got '" + std::to_string(dw) + "'\n";
    return;
  }

  // Checks that the step is not too big
  if (wf < w0 + dw)
  {
    errors += "Limits(): step is bigger than frequency range.\n";
    return;
  }
}
//----------------------------------------------------------------------
// Sets the name of the output file
//----------------------------------------------------------------------
void mofDF::Def_Outfile(clparser::csr s)
{
  if (s.empty())     // Arguments is an empty string: error...
  {
    errors += "Output(): File name required\n";
    return;
  }

  OutFile = s;
}
//----------------------------------------------------------------------
// Returns the angle vector for the chosen functions
//----------------------------------------------------------------------
std::vector<double> mofDF::Angles(unsigned Func) const
{
  switch (Func)
  {
    case al::R    : return angs_R;
    case al::loss : return angs_eta;
  }
}

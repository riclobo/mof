//----------------------------------------------------------------------
// Monoclinic optical functions
//----------------------------------------------------------------------
#ifndef MOF_FUNCS_H_INCLUDED
#define MOF_FUNCS_H_INCLUDED
//----------------------------------------------------------------------
//#include <iostream>
//#include <fstream>
//#include <sstream>
#include <string>
#include "mofdf.h"
#include "TMonoclinicDF.h"
//----------------------------------------------------------------------
   int ParseCommandFile(const std::string &fn);

  void GenerateOptFuncs(const mofDF &df);
  void SetHeadher(std::string &Header, const mofDF &df);
  void SetData(std::vector<std::string> &Data,
               const std::vector<double> &w,
               const std::vector<COMPLEX> exx,
               const std::vector<COMPLEX> eyy,
               const std::vector<COMPLEX> exy,
               const mofDF &df,
               const TMonoclinicDF &mdf);
  void AddToData(std::string &d,
                 const COMPLEX &e, const COMPLEX &s,
                 unsigned of);

//----------------------------------------------------------------------
#endif // MOF_FUNCS_H_INCLUDED
